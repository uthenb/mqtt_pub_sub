
//http://www.steves-internet-guide.com/using-node-mqtt-client/

var mqtt=require('mqtt');

var topic="testtopic";
var message="test message";
var topic_list=["topic2","topic3","topic4"];
var topic_o={"topic22":0,"topic33":1,"topic44":1};

var message="test message";
var count =0;

var options = {
    port: 18317,
    host: 'mqtt://soldier.cloudmqtt.com',
    clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
    username: 'xxxxx',
    password: 'xxxxx',
    keepalive: 60,
    reconnectPeriod: 1000,
    clean: true,
    encoding: 'utf8',
    retain:true,
    qos:1
};

var client = mqtt.connect("mqtt://soldier.cloudmqtt.com",options);

client.on("connect",function(){	
    console.log("connected");
});

client.on("error",function(error){ 
    console.log("Can't connect"+error)}
);

client.on('message',function(topic, message, packet){
	console.log("message is "+ message);
	console.log("topic is "+ topic);
});

console.log("subscribing to topics");
client.subscribe(topic,{qos:1}); //single topic
client.subscribe(topic_list,{qos:1}); //topic list
client.subscribe(topic_o); //object

//publish
function publish(topic,msg,options){
    console.log("publishing",msg);
    
    if (client.connected == true){        
        client.publish(topic,msg,options);
    }
    //count+=1;
    //if (count==2) //ens script
    //   clearTimeout(timer_id); //stop timer
    //client.end();	
}

var timer_id=setInterval(function(){publish(topic,message,options);},2000);